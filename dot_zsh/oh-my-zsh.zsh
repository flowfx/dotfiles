export ZSH=$HOME/.oh-my-zsh

ZSH_THEME="robbyrussell"

# How often to auto-update (in days)?
zstyle ':omz:update' frequency 23

# Enable command auto-correction.
ENABLE_CORRECTION="false"

# Display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

plugins=(direnv gh thefuck z)

source $ZSH/oh-my-zsh.sh
