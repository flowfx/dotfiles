setopt hist_ignore_all_dups inc_append_history
HISTFILE=~/.zhistory
HISTSIZE=4096
SAVEHIST=4096

# https://github.com/elixir-lang/elixir/wiki/FAQ#31-how-to-have-my-iex-session-history-to-be-persistent-over-different-iex-sessions
export ERL_AFLAGS="-kernel shell_history enabled"
